//
//  main.swift
//  Properties
//
//  Created by Albert Pangestu on 29/04/20.
//  Copyright © 2020 Stamina Left. All rights reserved.
//

import Foundation

var rangeOfThreeItems = FixedLengthRange(firstValue: 0, length: 3)

print(rangeOfThreeItems)

rangeOfThreeItems.firstValue = 6

print(rangeOfThreeItems)

let rangeOfFourItems = FixedLengthRange(firstValue: 0, length: 4)

print(rangeOfFourItems)

//rangeOfFourItems.firstValue = 6

let manager = DataManager()
manager.data.append("Some data")
manager.data.append("Some more data")

print(manager.importer.filename + "\n")

var square = Rect(origin: Point(x: 0.0, y: 0.0), size: Size(width: 10.0, height: 10.0))
let initialSquareCenter = square.center

print(initialSquareCenter)

square.center = Point(x: 15.0, y: 15.0)
print("square.origin is now at (\(square.origin.x), \(square.origin.y)) \n")

let fourByFiveByTwo = Cuboid(width: 4.0, height: 5.0, depth: 2.0)
print("the volume of fourByFiveByTwo is \(fourByFiveByTwo.volume)")

//
//  FixedLengthRange.swift
//  Properties
//
//  Created by Albert Pangestu on 29/04/20.
//  Copyright © 2020 Stamina Left. All rights reserved.
//

import Foundation

/**
 Ini struct untuk FixedLengthRange
 */
struct FixedLengthRange {
    var firstValue: Int
    let length: Int
}

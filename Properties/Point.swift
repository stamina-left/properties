//
//  Point.swift
//  Properties
//
//  Created by Albert Pangestu on 29/04/20.
//  Copyright © 2020 Stamina Left. All rights reserved.
//

import Foundation

struct Point {
    var x = 0.0, y = 0.0
}

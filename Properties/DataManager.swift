//
//  DataManager.swift
//  Properties
//
//  Created by Albert Pangestu on 29/04/20.
//  Copyright © 2020 Stamina Left. All rights reserved.
//

import Foundation

class DataManager {
    lazy var importer = DataImporter()
    var data = [String]()
}

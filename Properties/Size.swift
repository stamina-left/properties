//
//  Size.swift
//  Properties
//
//  Created by Albert Pangestu on 29/04/20.
//  Copyright © 2020 Stamina Left. All rights reserved.
//

import Foundation

struct Size {
    var width = 0.0, height = 0.0
}
